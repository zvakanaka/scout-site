# Advancement
In addition to weekly activities, the Scouts can work on all the Scout to 1st Class rank requirements during our 1 hour Weekly meetings except the ones listed below, which can be completed at home.

## Scout Rank 
[pdf](https://filestore.scouting.org/filestore/boyscouts/pdf/Scout_rank_2016.pdf)  
Requirement #7 (page 439 in book):
[http://www.scouting.org/filestore/ypt/pdf/46-015.pdf](http://www.scouting.org/filestore/ypt/pdf/46-015.pdf)

## Tenderfoot Rank
[pdf](https://filestore.scouting.org/filestore/boyscouts/pdf/Tenderfoot_rank_2016.pdf)  
Requirement #1b,2a,2b,9 (page 440 and 441 in book)

## 2nd Class Rank
[pdf](https://filestore.scouting.org/filestore/boyscouts/pdf/First_Class_rank_2017.pdf)  
Requirement #1a,1c,2e,3b,7a,7b,7c,8a,8c,8d,8e,10,11 (page 442, 443 and 444 in book)

## 1st Class Rank
[pdf](https://filestore.scouting.org/filestore/boyscouts/pdf/Second_Class_rank_2017.pdf)  
Requirement #1a,1b,2a,2b,2c,4a,7e,8a,8b,9a,9b,9c,9d,10,11,12 (page 445,446 and 447 in book)
